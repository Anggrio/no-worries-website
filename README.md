# CSCM603125 Software Engineering 2020 - Individual Project

#### No-Worries Website
#### Anggrio Wildanhadi Sutopo / 1806173544

## ToDo List

- [x] Initialize gitlab repository
- [x] Initialize Spring project using Spring Boot
- [x] Initialize Heroku Application
- [x] Implement design of first iteration
- [x] Implement design of second iteration

- [ ] Finalize project implementation

## Relevant Links
1. Heroku Website: [Heroku Website](https://no-worries.herokuapp.com/)
2. Work Document: [Google Docs](https://docs.google.com/document/d/1W6AfaCM7yr43CTO3mYt9urBQAU4vYb_2KHFtrxQTHH0/edit?usp=sharing)
3. Work Log: [Google Sheets](https://docs.google.com/spreadsheets/d/1NnSCJJiq-pyIRZM7qlSY-xF6EOkzjIu_4uuxeaajtWE/edit?usp=sharing)