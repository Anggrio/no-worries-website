package software.engineering.no.worries.website.service;

import software.engineering.no.worries.website.model.Status;

public interface StatusService {

    void addStatus(Status status);
    Status getRandomStatus();
}
