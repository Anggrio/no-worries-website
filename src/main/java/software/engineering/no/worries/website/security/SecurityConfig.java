package software.engineering.no.worries.website.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                        .antMatchers("/").authenticated()
                        .antMatchers("post-status").authenticated()
                        .anyRequest().authenticated()
                .and()
                .oauth2Login()
                        .loginPage("/login")
                        .defaultSuccessUrl("/")
                        .permitAll()
                .and()
                .logout()
                        .logoutSuccessUrl("/login")
                        .permitAll();
    }
}
