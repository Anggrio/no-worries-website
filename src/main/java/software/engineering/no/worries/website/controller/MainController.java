package software.engineering.no.worries.website.controller;

import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import software.engineering.no.worries.website.model.Status;
import software.engineering.no.worries.website.service.StatusService;

import java.security.Principal;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @Autowired
    private StatusService statusService;

    @GetMapping("/login")
    public String loginPage(Model model) {
        return "login";
    }

    @GetMapping("/")
    public String viewStatusPage(Principal principal, OAuth2AuthenticationToken authentication, Model model) {
        setUsernameModel(principal, authentication, model);
        Status status = statusService.getRandomStatus();
        if (status == null) {
            model.addAttribute("status", "No status available, check again later!");
        } else {
            model.addAttribute("status", status.getContent());
        }
        return "view-status";
    }

    @GetMapping("/post-status")
    public String postStatusPage(@ModelAttribute("status") Status status, Principal principal, OAuth2AuthenticationToken authentication, Model model) {
        setUsernameModel(principal, authentication, model);
        return "post-status";
    }

    @PostMapping("/post-status")
    public String postStatusPageSubmit(@ModelAttribute("status") Status status, RedirectAttributes redirectAttributes) {
        if (status.getContent().trim().isEmpty()) {
            redirectAttributes.addFlashAttribute("errorMsg", "Please enter a valid status");
            return "redirect:/post-status";
        }
        statusService.addStatus(status);
        return "redirect:/";
    }

    private void setUsernameModel(Principal principal, OAuth2AuthenticationToken authentication, Model model) {
        OAuth2AuthorizedClient client = authorizedClientService
                .loadAuthorizedClient(
                        authentication.getAuthorizedClientRegistrationId(),
                        authentication.getName());
        String userInfoEndpointUri = client.getClientRegistration()
                .getProviderDetails().getUserInfoEndpoint().getUri();

        if (!StringUtils.isEmpty(userInfoEndpointUri)) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken()
                    .getTokenValue());
            HttpEntity entity = new HttpEntity("", headers);
            ResponseEntity response = restTemplate
                    .exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
            Map userAttributes = (Map) response.getBody();
            model.addAttribute("username", userAttributes.get("name"));
        } else {
            model.addAttribute("username", principal.getName());
        }
    }
}
