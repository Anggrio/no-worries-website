package software.engineering.no.worries.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.engineering.no.worries.website.model.Status;
import software.engineering.no.worries.website.repository.StatusRepository;

import java.util.List;
import java.util.Random;

@Service
public class StatusServiceImpl implements StatusService {

    @Autowired
    private StatusRepository statusRepository;

    private Random randomizer = new Random();

    @Override
    public void addStatus(Status status) {
        statusRepository.save(status);
    }

    @Override
    public Status getRandomStatus() {
        List<Status> statusList = statusRepository.findAll();
        if (statusList.isEmpty()) {
            return null;
        } else {
            return statusList.get(randomizer.nextInt(statusList.size()));
        }
    }
}
